const path = require("path");
const os = require("os");
const { app, BrowserWindow, Menu, ipcMain, shell } = require("electron");
const imagemin = require("imagemin");
const imageminMozjpeg = require("imagemin-mozjpeg");
const imageminPngquant = require("imagemin-pngquant");
const slash = require("slash");
const log = require("electron-log");

process.env.NODE_ENV = "production";

const isDev = process.env.NODE_ENV !== "production" ? true : false;
const isMac = process.platform === "darwin" ? true : false;

let mainWindow;
let aboutWindow;

/*
* On créé la vue principale
*/
function createMainWindow() {
  mainWindow = new BrowserWindow({
    title: "ImageMinifier",           // Titre de la vue
    width: isDev ? 800 : 500,
    height: 600,
    icon: path.join(__dirname, "./assets/icons/Icon_256x256.png"),
    resizable: isDev ? true : false,
    backgroundColor: "white",
    webPreferences: {
      nodeIntegration: true,
    },
  });

  if (isDev) mainWindow.webContents.openDevTools();

  mainWindow.loadFile("./app/index.html");  // Charge le fichier HTML
}


/*
* On créé la vue "About"
*/
function createAboutWindow() {
  aboutWindow = new BrowserWindow({
    title: "About ImageMinifier",   // Titre de la vue
    width: 300,
    height: 300,
    icon: path.join(__dirname, "./assets/icons/Icon_256x256.png"),
    resizable: false,
    backgroundColor: "white",
  });

  aboutWindow.loadFile("./app/about.html");  // Charge le fichier HTML
}

app.on("ready", () => {
  createMainWindow();   // L'application est prête, on affiche la vue principale

  const mainMenu = Menu.buildFromTemplate(menu);  // On créé le menu
  Menu.setApplicationMenu(mainMenu);              // On assigne le menu à l'application

  mainWindow.on("ready", () => (mainMenu = null));
});

const menu = [
  ...(isMac
    ? [
        {
          label: app.name,
          submenu: [
            {
              label: "About",
              click: createAboutWindow,
            },
          ],
        },
      ]
    : []),
  {
    role: "fileMenu",
  },
  ...(!isMac
    ? [
        {
          label: "Help",
          submenu: [
            {
              label: "About",
              click: createAboutWindow,
            },
          ],
        },
      ]
    : []),
  ...(isDev
    ? [
        {
          label: "Developer",
          submenu: [
            { role: "reload" },
            { role: "forcereload" },
            { type: "separator" },
            { role: "toggledevtools" },
          ],
        },
      ]
    : []),
];

// Le front envoie la commande "image:minimize"
ipcMain.on("image:minimize", (e, opts) => {
  opts.dest = path.join(os.homedir(), "imageminifier");  // Emplacement où sera sauvegardé l'image ex: C:\\Users\<username>\imageshrink
  shrinkImage(opts);            // Appelle la fonction pour traiter l'image
});

/**
 * 
 * @param {string} imgPath
 * @param {number} quality
 * @param {string} dest 
 */
async function shrinkImage({ imgPath, quality, dest }) {
  try {
    const pngQuality = quality / 100;
    // traitement de l'image et sauvegarde
    const files = await imagemin([slash(imgPath)], {
      destination: dest,
      plugins: [
        imageminMozjpeg({ quality }),
        imageminPngquant({
          quality: [pngQuality, pngQuality],
        }),
      ],
    });
    log.info(files);
    shell.openPath(dest);     // Ouvre le dossier où le fichier a été sauvegardé
    mainWindow.webContents.send("image:done");  // Envoie la commande "image:done" au Front
  } catch (err) {
    log.error(err);
  }
}

app.on("window-all-closed", () => {
  if (!isMac) app.quit();
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) createMainWindow();
});

app.allowRendererProcessReuse = true;
