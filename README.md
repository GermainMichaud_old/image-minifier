# Image Minifier

Réduire la qualité de l'image pour en réduire sa taille.

### Installation

[Node.js](https://nodejs.org/) est requis.

```sh
$ git git@gitlab.com:GermainMichaud/image-minifier.git
$ cd ./image-minifier
$
$ npm install
ou
$ yarn
```

```sh
$ npm start
ou
$ yarn start
```

### Development

```sh
$ npm run dev
ou
$ yarn dev
```

### Build

> Pour Windows

```sh
$ npm run package-win
ou
$ yarn package-win
```

> Pour Mac

```sh
$ npm run package-mac
ou
$ yarn package-mac
```

> Pour Linux

```sh
$ npm run package-linux
ou
$ yarn package-linux
```

### Test

### Librairies

Image Minifier utilise plusieurs librairies Open Source

| Librairie                              | Version |
| -------------------------------------- | ------- |
| [electron][electron]                   | ^9.0.0  |
| [electron-packager][electron-packager] | ^14.2.1 |
| [nodemon][nodemon]                     | ^2.0.3  |
| [electron-log][electron-log]           | ^4.1.2  |
| [imagemin][imagemin]                   | ^7.0.1  |
| [imagemin-mozjpeg][imagemin-mozjpeg]   | ^8.0.0  |
| [imagemin-pngquant][imagemin-pngquant] | ^8.0.0  |
| [slash][slash]                         | ^3.0.0  |

### License

MIT

[//]: # 'Liens vers les repos utilisés'
[node.js]: http://nodejs.org
[electron]: https://github.com/electron/electron
[electron-packager]: https://github.com/electron/electron-packager
[nodemon]: https://github.com/remy/nodemon
[electron-log]: https://github.com/megahertz/electron-log
[imagemin]: https://github.com/imagemin/imagemin
[imagemin-mozjpeg]: https://github.com/imagemin/imagemin-mozjpeg
[imagemin-pngquant]: https://github.com/imagemin/imagemin-pngquant
[slash]: https://github.com/sindresorhus/slash
